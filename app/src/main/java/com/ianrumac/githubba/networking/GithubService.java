package com.ianrumac.githubba.networking;

import com.ianrumac.githubba.repodetails.models.network.GithubRepositoryDetailsResponse;
import com.ianrumac.githubba.search.models.network.GithubListResponseWrapper;
import com.ianrumac.githubba.userdetails.models.network.UserResponse;
import com.ianrumac.githubba.utils.Constants;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ianrumac on 08/04/2017.
 */

public interface GithubService {


	@GET(Constants.Endpoints.SEARCH_REPOSITORIES)
	Observable<GithubListResponseWrapper> getRepositoriesForQuery(
			@Query("q") String query,
			@Query("sort") String sort,
			@Query("order") String order,
			@Query("page") int page,
			@Query("per_page") int pageMax
	);

	@GET(Constants.Endpoints.USER_BY_USERNAME)
	Single<UserResponse> getUserByUsername(
			@Path("username") String username

	);

	@GET(Constants.Endpoints.REPOSITORY_BY_OWNER)
	Single<GithubRepositoryDetailsResponse> getRepositoryByOwnerAndName(
			@Path("owner") String owner,
			@Path("name") String repositoryName

	);


}
