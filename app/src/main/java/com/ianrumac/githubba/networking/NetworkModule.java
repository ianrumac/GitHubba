package com.ianrumac.githubba.networking;

import com.ianrumac.githubba.BuildConfig;
import com.ianrumac.githubba.CoreApplication;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ianrumac on 08/04/2017.
 */

@Module
public class NetworkModule {

	private static final String RESPONSE_DIR_NAME = "responses";
	private static final int CACHE_SIZE = 10 * 1024 * 1024;
	private static final String TOKEN_VALUE = "token ";
	private static final String AUTH = "Authorization";

	@Provides
	@Singleton
	public OkHttpClient provideHttpClient() {
		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
		loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		File httpCacheDirectory = new File(CoreApplication.getInstance().getCacheDir(), RESPONSE_DIR_NAME);
		Cache cache = new Cache(httpCacheDirectory, CACHE_SIZE);

		return new OkHttpClient.Builder()
				.addInterceptor(chain -> {
					Request request = chain.request().newBuilder()
							.addHeader(AUTH, TOKEN_VALUE
									.concat(BuildConfig.TOKEN)).build();
					return chain.proceed(request);
				})
				.addInterceptor(loggingInterceptor)
				.connectTimeout(20 * 1000, TimeUnit.MILLISECONDS)
				.readTimeout(30 * 1000, TimeUnit.MILLISECONDS)
				.cache(cache)
				.build();
	}

	@Provides
	@Singleton
	public Retrofit provideRestAdapter(OkHttpClient httpClient) {
		return new Retrofit.Builder()
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create())
				.client(httpClient)
				.baseUrl(BuildConfig.BASE_URL)
				.build();
	}

	@Provides
	@Singleton
	public GithubService provideRetrofitService(Retrofit retrofit) {
		return retrofit.create(GithubService.class);
	}
}
