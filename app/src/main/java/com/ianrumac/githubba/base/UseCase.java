package com.ianrumac.githubba.base;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by ianrumac on 08/04/2017.
 */

public abstract class UseCase<T, Params> {


	protected abstract Observable<T> executeWithParams(Params query);

	public void executeWithParams(Params query, Consumer<T> onNext, Consumer<Throwable> onError) {
		executeWithParams(query).subscribe(onNext, onError);

	}

	;
}
