package com.ianrumac.githubba.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by ianrumac on 09/04/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {


	public void setTextIfNotEmptyOrElse(@Nullable String text, TextView view, @NonNull String textIfEmpty) {
		if (!TextUtils.isEmpty(text)) {
			view.setText(text);
		} else if (!textIfEmpty.isEmpty()) {
			view.setText(textIfEmpty);
		}
	}

	public void setTextIfNotEmptyOrElse(@Nullable String text, TextView view, @StringRes int textIfEmpty) {
		setTextIfNotEmptyOrElse(text, view, getString(textIfEmpty));
	}


}
