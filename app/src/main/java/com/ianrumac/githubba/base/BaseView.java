package com.ianrumac.githubba.base;

/**
 * Created by ianrumac on 08/04/2017.
 */

public interface BaseView {

	void showLoading();

	void hideLoading();

	void displayError();
}
