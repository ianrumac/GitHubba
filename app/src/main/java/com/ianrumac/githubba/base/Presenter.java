package com.ianrumac.githubba.base;

/**
 * Created by ianrumac on 08/04/2017.
 */

public interface Presenter<T extends BaseView> {

	void attachView(T view);

	void detachView(T view);

	void handleError(Throwable throwable);
}
