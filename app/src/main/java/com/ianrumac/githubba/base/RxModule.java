package com.ianrumac.githubba.base;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ianrumac on 08/04/2017.
 */

@Module
public class RxModule {

	@Provides
	@Named("post_execution")
	public Scheduler provideMainThreadScheduler() {
		return AndroidSchedulers.mainThread();
	}

	@Provides
	@Named("io")
	public Scheduler provideIOScheduler() {
		return Schedulers.io();
	}

}
