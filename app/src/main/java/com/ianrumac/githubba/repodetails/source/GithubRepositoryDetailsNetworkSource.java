package com.ianrumac.githubba.repodetails.source;

import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.repodetails.mappers.GithubRepositoryDetailsMapper;
import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class GithubRepositoryDetailsNetworkSource implements GithubRepositoryDetailsDataSource {


	GithubService apiService;
	GithubRepositoryDetailsMapper mapper;

	@Inject
	public GithubRepositoryDetailsNetworkSource(GithubService apiService, GithubRepositoryDetailsMapper mapper) {
		this.apiService = apiService;
		this.mapper = mapper;
	}


	@Override
	public Single<GithubRepositoryInfo> getRepositoryDetails(String owner, String repositoryName) {
		return mapper.mapResponseResultToList(apiService.getRepositoryByOwnerAndName(owner, repositoryName));
	}
}
