package com.ianrumac.githubba.repodetails.source;

import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;

import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public interface GithubRepositoryDetailsDataSource {

	Single<GithubRepositoryInfo> getRepositoryDetails(String owner, String repositoryName);
}
