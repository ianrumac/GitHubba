package com.ianrumac.githubba.repodetails;

import com.ianrumac.githubba.base.BaseView;
import com.ianrumac.githubba.base.Presenter;
import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;

/**
 * Created by ianrumac on 09/04/2017.
 */

public interface RepositoryDetailsContract {


	interface RepositoryDetailsView extends BaseView {

		void showRepositoryDetails(GithubRepositoryInfo githubRepositoryInfo);


	}

	interface RepositoryDetailsPresenter extends Presenter<RepositoryDetailsView> {

		void fetchAndDisplayRepositoryData(String owner, String repositoryName);

		void displayOwnerDetails(String owner);

		void displayFullRepository(String url);

	}

}
