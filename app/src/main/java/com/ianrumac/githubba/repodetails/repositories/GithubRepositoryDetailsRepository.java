package com.ianrumac.githubba.repodetails.repositories;

import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;
import com.ianrumac.githubba.repodetails.source.GithubRepositoryDetailsDataSource;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class GithubRepositoryDetailsRepository implements RepositoryDetailsRepository {

	GithubRepositoryDetailsDataSource dataSource;
	Scheduler executionScheduler;

	@Inject
	public GithubRepositoryDetailsRepository(GithubRepositoryDetailsDataSource dataSource,
											 @Named("io") Scheduler execution) {
		this.dataSource = dataSource;
		this.executionScheduler = execution;
	}

	@Override
	public Single<GithubRepositoryInfo> fetchRepositoryDetails(String owner, String repositoryName) {
		return dataSource.getRepositoryDetails(owner, repositoryName).subscribeOn(executionScheduler);
	}
}
