package com.ianrumac.githubba.repodetails.models.network;

import com.google.gson.annotations.SerializedName;
import com.ianrumac.githubba.search.models.network.GithubRepositoryOwnerResponse;

import java.util.List;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class GithubRepositoryDetailsResponse {


	/**
	 * id : 1296269
	 * name : Hello-World
	 * full_name : octocat/Hello-World
	 * description : This your first repo!
	 * fork : false
	 * html_url : https://github.com/octocat/Hello-World
	 * language : null
	 * forks_count : 9
	 * stargazers_count : 80
	 * watchers_count : 80
	 * size : 108
	 * default_branch : master
	 * open_issues_count : 0
	 * topics : ["octocat","atom","electron","API"]
	 * pushed_at : 2011-01-26T19:06:43Z
	 * created_at : 2011-01-26T19:01:12Z
	 * updated_at : 2011-01-26T19:14:43Z
	 * subscribers_count : 42
	 * network_count : 0
	 */

	@SerializedName("id") private int id;
	@SerializedName("name") private String name;
	@SerializedName("full_name") private String fullName;
	@SerializedName("description") private String description;
	@SerializedName("fork") private boolean fork;
	@SerializedName("html_url") private String htmlUrl;
	@SerializedName("language") private String language;
	@SerializedName("forks_count") private int forksCount;
	@SerializedName("stargazers_count") private int stargazersCount;
	@SerializedName("owner") private GithubRepositoryOwnerResponse itemOwner;
	@SerializedName("watchers_count") private int watchersCount;
	@SerializedName("size") private int size;
	@SerializedName("open_issues_count") private int openIssues;
	@SerializedName("pushed_at") private String pushedAt;
	@SerializedName("created_at") private String createdAt;
	@SerializedName("updated_at") private String updatedAt;
	@SerializedName("subscribers_count") private int subscribersCount;
	@SerializedName("network_count") private int networkCount;
	@SerializedName("topics") private List<String> topics;

	public int getId() {
		return id;
	}


	public String getName() {
		return name;
	}

	public String getFullName() {
		return fullName;
	}


	public String getDescription() {
		return description;
	}


	public boolean isFork() {
		return fork;
	}


	public String getHtmlUrl() {
		return htmlUrl;
	}


	public String getLanguage() {
		return language;
	}


	public int getForksCount() {
		return forksCount;
	}


	public int getStargazersCount() {
		return stargazersCount;
	}


	public int getWatchersCount() {
		return watchersCount;
	}


	public int getSize() {
		return size;
	}


	public int getOpenIssues() {
		return openIssues;
	}


	public String getPushedAt() {
		return pushedAt;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public String getUpdatedAt() {
		return updatedAt;
	}


	public int getSubscribersCount() {
		return subscribersCount;
	}


	public int getNetworkCount() {
		return networkCount;
	}


	public List<String> getTopics() {
		return topics;
	}

	public GithubRepositoryOwnerResponse getItemOwner() {
		return itemOwner;
	}
}
