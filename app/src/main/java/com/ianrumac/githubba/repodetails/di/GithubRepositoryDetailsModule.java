package com.ianrumac.githubba.repodetails.di;

import com.ianrumac.githubba.core.di.PerFragment;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.repodetails.RepositoryDetailsContract;
import com.ianrumac.githubba.repodetails.interactors.GetRepositoryDetailsUseCase;
import com.ianrumac.githubba.repodetails.mappers.GithubRepositoryDetailsMapper;
import com.ianrumac.githubba.repodetails.repositories.GithubRepositoryDetailsRepository;
import com.ianrumac.githubba.repodetails.repositories.RepositoryDetailsRepository;
import com.ianrumac.githubba.repodetails.source.GithubRepositoryDetailsDataSource;
import com.ianrumac.githubba.repodetails.source.GithubRepositoryDetailsNetworkSource;
import com.ianrumac.githubba.userdetails.RepositoryDetailsPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */


@Module
public class GithubRepositoryDetailsModule {


	@Provides
	@PerFragment
	GithubRepositoryDetailsMapper provideResultMapper() {
		return new GithubRepositoryDetailsMapper();
	}

	@Provides
	@PerFragment
	GithubRepositoryDetailsDataSource provideSource(GithubService service, GithubRepositoryDetailsMapper mapper) {
		return new GithubRepositoryDetailsNetworkSource(service, mapper);
	}

	@Provides
	@PerFragment
	RepositoryDetailsRepository provideRepositoryDetailsRepository(GithubRepositoryDetailsDataSource source, @Named("io") Scheduler io) {
		return new GithubRepositoryDetailsRepository(source, io);
	}

	@Provides
	@PerFragment
	GetRepositoryDetailsUseCase provideGetRepositoryDetailsUseCase(RepositoryDetailsRepository repository) {
		return new GetRepositoryDetailsUseCase(repository);
	}

	@Provides
	@PerFragment
	RepositoryDetailsContract.RepositoryDetailsPresenter providePresenter(GetRepositoryDetailsUseCase repositoriesUseCase,
																		  @Named("post_execution") Scheduler postExecution,
																		  Router router) {
		return new RepositoryDetailsPresenter(postExecution, repositoriesUseCase, router);
	}


}
