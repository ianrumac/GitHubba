package com.ianrumac.githubba.repodetails.repositories;

import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;

import io.reactivex.Single;

/**
 * Created by ianrumac on 10/04/2017.
 */
public interface RepositoryDetailsRepository {
	Single<GithubRepositoryInfo> fetchRepositoryDetails(String owner, String repositoryName);
}
