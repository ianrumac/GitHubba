package com.ianrumac.githubba.repodetails.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ianrumac.githubba.R;
import com.ianrumac.githubba.base.BaseFragment;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.repodetails.RepositoryDetailsContract;
import com.ianrumac.githubba.repodetails.di.DaggerRepositoryDetailsComponent;
import com.ianrumac.githubba.repodetails.di.GithubRepositoryDetailsModule;
import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;
import com.ianrumac.githubba.search.di.SearchComponentOwner;
import com.ianrumac.githubba.utils.DateTimeUtils;
import com.ianrumac.githubba.utils.ImageLoader;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RepositoryDetailsFragment extends BaseFragment implements RepositoryDetailsContract.RepositoryDetailsView {


	private static final String OWNER = "owner";
	private static final String REPOSITORY_NAME = "repo-name";

	@BindView(R.id.user_name) TextView userNameView;
	@BindView(R.id.user_photo) ImageView userPhoto;
	@BindView(R.id.title) TextView title;
	@BindView(R.id.watchers) TextView watchers;
	@BindView(R.id.forks) TextView forks;
	@BindView(R.id.language) TextView language;
	@BindView(R.id.created_at) TextView createdAt;
	@BindView(R.id.last_update) TextView lastUpdate;
	@BindView(R.id.issues) TextView issues;
	@BindView(R.id.link) TextView link;

	@Inject RepositoryDetailsContract.RepositoryDetailsPresenter presenter;
	@Inject Router router;
	@Inject ImageLoader loader;
	private String owner;
	private String repositoryName;

	public static RepositoryDetailsFragment newInstance(String owner, String repository) {
		RepositoryDetailsFragment fragment = new RepositoryDetailsFragment();
		Bundle args = new Bundle();
		args.putString(OWNER, owner);
		args.putString(REPOSITORY_NAME, repository);
		fragment.setArguments(args);
		return fragment;
	}

	public RepositoryDetailsFragment() {
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			owner = getArguments().getString(OWNER);
			repositoryName = getArguments().getString(REPOSITORY_NAME);
		} else {
			throw new IllegalStateException("Owner and repository name must be passed");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_repository_details, container, false);
		ButterKnife.bind(this, view);
		DaggerRepositoryDetailsComponent.builder().searchComponent(((SearchComponentOwner) getActivity()).searchComponent())
				.githubRepositoryDetailsModule(new GithubRepositoryDetailsModule())
				.build()
				.inject(this);
		presenter.attachView(this);
		presenter.fetchAndDisplayRepositoryData(owner, repositoryName);
		return view;


	}

	@Override
	public void showLoading() {
		userNameView.setText(R.string.loading);
		watchers.setText(R.string.loading);
		forks.setText(R.string.loading);

	}

	@Override
	public void hideLoading() {

	}

	@Override
	public void displayError() {
		Toast.makeText(getContext(), R.string.error_msg, Toast.LENGTH_LONG).show();
	}


	@Override
	public void onDestroyView() {
		presenter.detachView(this);
		super.onDestroyView();
	}

	@Override
	public void showRepositoryDetails(GithubRepositoryInfo repository) {

		loader.loadImage(userPhoto, repository.getAuthor().getAvatarURL());

		title.setText(repository.getName());
		userNameView.setText(repository.getAuthor().getUsername());
		watchers.setText(String.valueOf(repository.getWatchersCount()));
		forks.setText(String.valueOf(repository.getForkCount()));
		issues.setText(String.valueOf(repository.getIssueCount()));

		setTextIfNotEmptyOrElse(
				repository.getLanguage(), language, R.string.unknown);
		setTextIfNotEmptyOrElse(
				DateTimeUtils.dayMonthYearFromISODateTime(repository.getCreatedAt()), createdAt, R.string.unknown);
		setTextIfNotEmptyOrElse(
				DateTimeUtils.dayMonthYearFromISODateTime(repository.getUpdatedAt()), lastUpdate, R.string.unknown);
		userNameView.setOnClickListener(view -> presenter.displayOwnerDetails(repository.getAuthor().getUsername()));
		userPhoto.setOnClickListener(view -> presenter.displayOwnerDetails(repository.getAuthor().getUsername()));
		link.setOnClickListener(v -> presenter.displayFullRepository(repository.getHtmlUrl()));

	}


	@OnClick(R.id.back)
	public void goBack() {
		router.goBack();
	}

}
