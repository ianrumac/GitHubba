package com.ianrumac.githubba.repodetails.mappers;

import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;
import com.ianrumac.githubba.repodetails.models.network.GithubRepositoryDetailsResponse;
import com.ianrumac.githubba.search.models.Owner;
import com.ianrumac.githubba.search.models.network.GithubRepositoryOwnerResponse;

import io.reactivex.Single;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class GithubRepositoryDetailsMapper {

	public Single<GithubRepositoryInfo> mapResponseResultToList(Single<GithubRepositoryDetailsResponse> details) {
		return details.map(response -> new GithubRepositoryInfo.Builder()
				.id(response.getId())
				.name(response.getName())
				.author(ownerFromItemOwner(response.getItemOwner()))
				.forkCount(response.getForksCount())
				.htmlUrl(response.getHtmlUrl())
				.language(response.getLanguage())
				.stargazersCount(response.getStargazersCount())
				.watchersCount(response.getWatchersCount())
				.createdAt(response.getCreatedAt())
				.updatedAt(response.getUpdatedAt()).build());

	}

	private Owner ownerFromItemOwner(GithubRepositoryOwnerResponse itemOwner) {
		return new Owner(itemOwner.
				getLogin(),
				itemOwner.getId(), itemOwner.getAvatarUrl());
	}

}
