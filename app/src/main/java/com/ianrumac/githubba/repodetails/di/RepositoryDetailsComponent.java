package com.ianrumac.githubba.repodetails.di;

import com.ianrumac.githubba.core.di.PerFragment;
import com.ianrumac.githubba.repodetails.ui.RepositoryDetailsFragment;
import com.ianrumac.githubba.search.di.SearchComponent;

import dagger.Component;

/**
 * Created by ianrumac on 08/04/2017.
 */

@PerFragment
@Component(dependencies = SearchComponent.class, modules = GithubRepositoryDetailsModule.class)
public interface RepositoryDetailsComponent {
	void inject(RepositoryDetailsFragment repositoryDetailsFragment);
}
