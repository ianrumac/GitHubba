package com.ianrumac.githubba.repodetails.interactors;

import com.ianrumac.githubba.base.UseCase;
import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;
import com.ianrumac.githubba.repodetails.repositories.RepositoryDetailsRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class GetRepositoryDetailsUseCase extends UseCase<GithubRepositoryInfo, GetRepositoryDetailsUseCase.Params> {

	private final RepositoryDetailsRepository repository;

	@Inject
	public GetRepositoryDetailsUseCase(RepositoryDetailsRepository repository) {
		this.repository = repository;
	}

	@Override
	public Observable<GithubRepositoryInfo> executeWithParams(Params query) {
		return repository.fetchRepositoryDetails(query.owner, query.name).toObservable();
	}

	public static final class Params {
		final String owner;
		final String name;

		public Params(String owner, String name) {
			this.owner = owner;
			this.name = name;
		}
	}
}
