package com.ianrumac.githubba.repodetails.models;

import com.ianrumac.githubba.search.models.Owner;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class GithubRepositoryInfo {

	private final int id;

	private final String name;

	private final Owner author;

	private final int watchersCount;

	private final int forkCount;

	private final int issueCount;
	private final int stargazersCount;
	private final String htmlUrl;
	private final String language;
	private final String updatedAt;
	private final String createdAt;

	private GithubRepositoryInfo(Builder builder) {
		id = builder.id;
		name = builder.name;
		author = builder.author;
		watchersCount = builder.watchersCount;
		forkCount = builder.forkCount;
		issueCount = builder.issueCount;
		stargazersCount = builder.stargazersCount;
		htmlUrl = builder.htmlUrl;
		language = builder.language;
		updatedAt = builder.updatedAt;
		createdAt = builder.createdAt;
	}


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Owner getAuthor() {
		return author;
	}

	public int getWatchersCount() {
		return watchersCount;
	}

	public int getForkCount() {
		return forkCount;
	}

	public int getIssueCount() {
		return issueCount;
	}

	public String getHtmlUrl() {
		return htmlUrl;
	}

	public String getLanguage() {
		return language;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public static final class Builder {
		private int id = 0;
		private String name = "";
		private Owner author = new Owner("", 0, "");
		private int watchersCount = 0;
		private int forkCount = 0;
		private int issueCount = 0;
		private int stargazersCount = 0;
		private String htmlUrl = "";
		private String language = "";
		private String updatedAt = "";
		private String createdAt = "";

		public Builder() {
		}

		public Builder id(int val) {
			id = val;
			return this;
		}

		public Builder name(String val) {
			name = val;
			return this;
		}

		public Builder author(Owner val) {
			author = val;
			return this;
		}

		public Builder watchersCount(int val) {
			watchersCount = val;
			return this;
		}

		public Builder forkCount(int val) {
			forkCount = val;
			return this;
		}

		public Builder issueCount(int val) {
			issueCount = val;
			return this;
		}

		public Builder stargazersCount(int val) {
			stargazersCount = val;
			return this;
		}

		public Builder htmlUrl(String val) {
			htmlUrl = val;
			return this;
		}

		public Builder language(String val) {
			language = val;
			return this;
		}

		public Builder updatedAt(String val) {
			updatedAt = val;
			return this;
		}

		public Builder createdAt(String val) {
			createdAt = val;
			return this;
		}

		public GithubRepositoryInfo build() {
			return new GithubRepositoryInfo(this);
		}
	}
}
