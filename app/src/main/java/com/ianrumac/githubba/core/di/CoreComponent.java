package com.ianrumac.githubba.core.di;

import com.ianrumac.githubba.CoreApplication;
import com.ianrumac.githubba.base.RxModule;
import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.networking.NetworkModule;
import com.ianrumac.githubba.utils.ImageLoader;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */

@Singleton
@Component(modules = {CoreModule.class, NetworkModule.class, RxModule.class})
public interface CoreComponent {

	CoreApplication application();

	GithubService service();

	ImageLoader imageLoader();

	@Named("io")
	Scheduler schedulerIO();

	@Named("post_execution")
	Scheduler schedulerPostExecution();

}
