package com.ianrumac.githubba.core.di;

import com.ianrumac.githubba.CoreApplication;
import com.ianrumac.githubba.utils.GlideImageLoader;
import com.ianrumac.githubba.utils.ImageLoader;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ianrumac on 08/04/2017.
 */
@Module
public class CoreModule {


	private CoreApplication application;

	public CoreModule(CoreApplication application) {
		this.application = application;
	}

	@Provides
	@Singleton
	CoreApplication getApplication() {
		return application;
	}

	@Provides
	@Singleton
	ImageLoader provideImageLoader(CoreApplication application) {
		return new GlideImageLoader(application);
	}
}
