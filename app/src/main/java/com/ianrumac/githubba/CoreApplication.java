package com.ianrumac.githubba;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.ianrumac.githubba.base.RxModule;
import com.ianrumac.githubba.core.di.CoreComponent;
import com.ianrumac.githubba.core.di.CoreModule;
import com.ianrumac.githubba.core.di.DaggerCoreComponent;
import com.ianrumac.githubba.networking.NetworkModule;
import com.squareup.leakcanary.LeakCanary;

import timber.log.Timber;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class CoreApplication extends Application {

	private static CoreApplication instance;
	private CoreComponent component;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		component = DaggerCoreComponent.builder().coreModule(new CoreModule(this)).networkModule(new NetworkModule()).rxModule(new RxModule()).build();

		if (BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
			Stetho.initializeWithDefaults(this);
			LeakCanary.install(this);
		}


	}

	public CoreComponent getComponent() {
		return component;
	}

	public static CoreApplication getInstance() {
		return instance;
	}
}
