package com.ianrumac.githubba.search.ui.viewmodels;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.epoxy.EpoxyHolder;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.ianrumac.githubba.R;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.utils.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchResultViewModel extends EpoxyModelWithHolder<SearchResultViewModel.SearchItemViewHolder> {


	ImageLoader loader;
	GithubRepository result;
	View.OnClickListener userDetailsListener;
	View.OnClickListener repoDetailsListener;

	public SearchResultViewModel(GithubRepository result,
								 View.OnClickListener userDetailsListener,
								 View.OnClickListener repoDetailsListener, ImageLoader loader) {
		this.result = result;
		this.loader = loader;
		this.userDetailsListener = userDetailsListener;
		this.repoDetailsListener = repoDetailsListener;

	}

	@Override
	protected SearchItemViewHolder createNewHolder() {
		return new SearchItemViewHolder();
	}

	@Override
	protected int getDefaultLayout() {
		return R.layout.search_list_viewmodel;
	}

	@Override
	public void bind(SearchItemViewHolder holder) {
		loader.loadImageWithPlaceHolderCenterCrop(holder.userPhoto, result.getAuthor().getAvatarURL(), R.color.black_alpha_20);
		holder.title.setText(result.getName());
		holder.userName.setText(result.getAuthor().getUsername());
		holder.userPhoto.setOnClickListener(userDetailsListener);
		holder.userName.setText(result.getAuthor().getUsername());

		holder.parentLayout.setOnClickListener(repoDetailsListener);
		holder.watchers.setText(String.valueOf(result.getWatchersCount()));
		holder.issues.setText(String.valueOf(result.getIssueCount()));
		holder.forks.setText(String.valueOf(result.getForkCount()));
	}

	static class SearchItemViewHolder extends EpoxyHolder {
		@BindView(R.id.user_photo) ImageView userPhoto;
		@BindView(R.id.title) TextView title;
		@BindView(R.id.user_name) TextView userName;
		@BindView(R.id.watchers) TextView watchers;
		@BindView(R.id.forks) TextView forks;
		@BindView(R.id.issues) TextView issues;
		@BindView(R.id.parent_layout) CardView parentLayout;


		@Override
		protected void bindView(View itemView) {
			ButterKnife.bind(this, itemView);
		}
	}
}
