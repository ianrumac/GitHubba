package com.ianrumac.githubba.search.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ianrumac.githubba.CoreApplication;
import com.ianrumac.githubba.R;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.search.SearchContract;
import com.ianrumac.githubba.search.di.DaggerSearchComponent;
import com.ianrumac.githubba.search.di.SearchComponent;
import com.ianrumac.githubba.search.di.SearchComponentOwner;
import com.ianrumac.githubba.search.di.SearchModule;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.models.Order;
import com.ianrumac.githubba.search.models.SearchParams;
import com.ianrumac.githubba.search.models.SortBy;
import com.ianrumac.githubba.search.ui.adapters.SearchResultsEpoxyAdapter;
import com.ianrumac.githubba.utils.Constants;
import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent;
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class SearchActivity extends AppCompatActivity implements SearchContract.SearchView, SearchComponentOwner {

	@BindView(R.id.recycler_result_list) RecyclerView recyclerResultList;
	@BindView(R.id.editText) EditText editText;
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.sort_button) ImageView sortButton;
	@BindView(R.id.sort_group) RadioGroup sortGroup;
	@Inject SearchContract.SearchPresenter presenter;
	@Inject Router router;
	@Inject SearchResultsEpoxyAdapter adapter;


	PublishSubject<Order> orderPublishSubject = PublishSubject.create();
	PublishSubject<SortBy> sortPublishSubject = PublishSubject.create();

	public static final String IS_SORT_SELECTED_KEY = "sort-selected";
	SearchComponent searchComponent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		searchComponent = DaggerSearchComponent.builder()
				.coreComponent(CoreApplication.getInstance().getComponent())
				.searchModule(new SearchModule(this, getSupportFragmentManager()))
				.build();
		searchComponent.inject(this);
		presenter.attachView(this);


		sortButton.setOnClickListener(v -> updateButtonStateAndEmmit(v, v.isSelected()));
		sortGroup.setOnCheckedChangeListener((radioGroup, id) -> checkViewsAndEmmitState(id));
		setSupportActionBar(toolbar);
		setupRecyclerViewWithAdapter();
		if (savedInstanceState == null) {
			emmitDefaultStates();
		} else {
			recoverFromConfigChange(savedInstanceState);
		}
	}

	private void setupRecyclerViewWithAdapter() {
		recyclerResultList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		recyclerResultList.setAdapter(adapter);
	}

	private void emmitDefaultStates() {
		sortPublishSubject.onNext(SortBy.STARS);
		orderPublishSubject.onNext(Order.DESCENDING);
	}

	private void recoverFromConfigChange(Bundle savedInstanceState) {
		checkViewsAndEmmitState(sortGroup.getCheckedRadioButtonId());
		updateButtonStateAndEmmit(sortButton, !savedInstanceState.getBoolean(IS_SORT_SELECTED_KEY, false));
	}

	private void checkViewsAndEmmitState(int id) {
		switch (id) {
			case R.id.btn_forks:
				sortPublishSubject.onNext(SortBy.FORKS);
				break;
			case R.id.btn_stars:
				sortPublishSubject.onNext(SortBy.STARS);
				break;
			case R.id.updated:
				sortPublishSubject.onNext(SortBy.UPDATED);
				break;
		}
	}

	private void updateButtonStateAndEmmit(View view, boolean isSelected) {

		if (isSelected) {
			view.setSelected(false);
		} else {
			view.setSelected(true);
		}
		emmitState(view.isSelected());
	}

	private void emmitState(boolean isViewSelected) {

		if (isViewSelected) {
			orderPublishSubject.onNext(Order.ASCENDING);
		} else {
			orderPublishSubject.onNext(Order.DESCENDING);
		}
	}


	@Override
	public void showLoading() {
	}

	@Override
	public void hideLoading() {

	}

	@Override
	public void displayResults(List<GithubRepository> results) {
		adapter.clearAndAddNewModels(results);
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(IS_SORT_SELECTED_KEY, sortButton.isSelected());
		super.onSaveInstanceState(outState);
	}

	@Override
	public void appendSearchResults(List<GithubRepository> results) {
		adapter.addModels(results);
	}

	public ObservableSource<Integer> paginationObserver() {
		return RxRecyclerView.scrollEvents(recyclerResultList)
				.debounce(300, TimeUnit.MILLISECONDS)
				.filter(recyclerViewScrollEvent -> recyclerViewScrollEvent.dy() > 0)
				.filter(recyclerViewScrollEvent -> adapter.getItemCount() % Constants.PER_PAGE_MAX == 0) // if it's odd, no more prefetching should be done
				.filter(recyclerViewScrollEvent -> {
					int lastVisibleItem = ((LinearLayoutManager) recyclerResultList.getLayoutManager()).findLastVisibleItemPosition();
					return adapter.getItemCount() <= lastVisibleItem * 2;
				})
				.flatMap(new Function<RecyclerViewScrollEvent, ObservableSource<Integer>>() {
					@Override
					public ObservableSource<Integer> apply(RecyclerViewScrollEvent recyclerViewScrollEvent) throws Exception {
						int pageNumber = adapter.getItemCount() / Constants.PER_PAGE_MAX + 1;

						Timber.e("pageNum" + pageNumber);
						return Observable.just(pageNumber);
					}
				})
				.filter(pageNumber -> pageNumber > 1);
	}

	@Override
	public Observable<SearchParams> getSearchObservable() {
		return Observable.combineLatest(RxTextView.textChanges(editText), orderPublishSubject, sortPublishSubject,
				(charSequence, order1, sortBy1) -> new SearchParams(charSequence.toString(), sortBy1, order1, 1));
	}

	@Override
	public Observable<SearchParams> getPaginatedObservable() {
		return Observable.combineLatest(RxTextView.textChanges(editText), orderPublishSubject, sortPublishSubject, paginationObserver(),
				(charSequence, order1, sortBy1, scrollingPage) -> new SearchParams(charSequence.toString(), sortBy1, order1, scrollingPage));

	}

	@Override
	public void displayEmpty() {

	}

	@Override
	public void displayError() {
		Toast.makeText(this, R.string.error_msg, Toast.LENGTH_LONG).show();
	}

	@Override
	public SearchComponent searchComponent() {
		return searchComponent;
	}
}
