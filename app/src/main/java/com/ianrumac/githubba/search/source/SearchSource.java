package com.ianrumac.githubba.search.source;

import com.ianrumac.githubba.search.models.GithubRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 08/04/2017.
 */

public interface SearchSource {

	public Observable<List<GithubRepository>> searchRepositoriesByQuery(String query, String sort, String order, int page);

}
