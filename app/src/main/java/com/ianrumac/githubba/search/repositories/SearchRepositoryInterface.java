package com.ianrumac.githubba.search.repositories;

import com.ianrumac.githubba.search.models.GithubRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 10/04/2017.
 */
public interface SearchRepositoryInterface {
	Observable<List<GithubRepository>> getRepositoriesByQuery(String query, String sort, String order, int page);
}
