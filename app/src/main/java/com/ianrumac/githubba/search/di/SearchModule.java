package com.ianrumac.githubba.search.di;

import android.app.Activity;
import android.support.v4.app.FragmentManager;

import com.ianrumac.githubba.core.di.PerActivity;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.navigation.RouterImpl;
import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.search.SearchContract;
import com.ianrumac.githubba.search.SearchPresenter;
import com.ianrumac.githubba.search.interactors.SearchRepositoriesUseCase;
import com.ianrumac.githubba.search.mappers.SearchResultMapper;
import com.ianrumac.githubba.search.repositories.SearchRepository;
import com.ianrumac.githubba.search.repositories.SearchRepositoryInterface;
import com.ianrumac.githubba.search.source.SearchNetworkSource;
import com.ianrumac.githubba.search.source.SearchSource;
import com.ianrumac.githubba.search.ui.adapters.SearchResultsAdapter;
import com.ianrumac.githubba.search.ui.adapters.SearchResultsEpoxyAdapter;
import com.ianrumac.githubba.utils.ImageLoader;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */


@Module
public class SearchModule {
	private final Activity activity;
	private final FragmentManager supportManger;

	public SearchModule(Activity activity, FragmentManager manager) {
		this.activity = activity;
		this.supportManger = manager;
	}

	@Provides
	@PerActivity
	SearchResultMapper provideResultMapper() {
		return new SearchResultMapper();
	}

	@Provides
	@PerActivity
	SearchSource provideSource(GithubService service, SearchResultMapper mapper) {
		return new SearchNetworkSource(service, mapper);
	}

	@Provides
	@PerActivity
	SearchRepositoryInterface provideRepository(SearchSource source, @Named("io") Scheduler io) {
		return new SearchRepository(source, io);
	}

	@Provides
	@PerActivity
	SearchRepositoriesUseCase provideSearchRepositoryUseCase(SearchRepositoryInterface repository) {
		return new SearchRepositoriesUseCase(repository);
	}

	@Provides
	@PerActivity
	SearchContract.SearchPresenter providePresenter(SearchRepositoriesUseCase repositoriesUseCase, @Named("post_execution") Scheduler postExecution) {
		return new SearchPresenter(repositoriesUseCase, postExecution);
	}

	@Provides
	@PerActivity
	Router provideRouter() {
		return new RouterImpl(activity, supportManger);
	}

	@Provides
	@PerActivity
	SearchResultsAdapter provideAdapter(Router router, ImageLoader loader) {
		return new SearchResultsEpoxyAdapter(router, loader);
	}

}
