package com.ianrumac.githubba.search.models;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class Owner {

	private final String username;
	private final long id;
	private final String avatarURL;

	public Owner(String username, long id, String avatarURL) {
		this.username = username;
		this.id = id;
		this.avatarURL = avatarURL;
	}

	public String getUsername() {
		return username;
	}

	public long getId() {
		return id;
	}

	public String getAvatarURL() {
		return avatarURL;
	}

	@Override
	public String toString() {
		return "Owner{" +
				"username='" + username + '\'' +
				", id=" + id +
				", avatarURL='" + avatarURL + '\'' +
				'}';
	}
}
