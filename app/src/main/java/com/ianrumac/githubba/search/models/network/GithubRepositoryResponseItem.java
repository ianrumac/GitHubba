package com.ianrumac.githubba.search.models.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class GithubRepositoryResponseItem {


	/**
	 * id : 3081286
	 * name : Tetris
	 * full_name : dtrupenn/Tetris
	 * private final : false
	 * html_url : https://github.com/dtrupenn/Tetris
	 * description : A C implementation of Tetris using Pennsim through LC4
	 * fork : false
	 * url : https://api.github.com/repos/dtrupenn/Tetris
	 * created_at : 2012-01-01T00:31:50Z
	 * updated_at : 2013-01-05T17:58:47Z
	 * pushed_at : 2012-01-01T00:37:02Z
	 * homepage :
	 * size : 524
	 * stargazers_count : 1
	 * watchers_count : 1
	 * language : Assembly
	 * forks_count : 0
	 * open_issues_count : 0
	 * master_branch : master
	 * default_branch : master
	 * score : 10.309712
	 */

	@SerializedName("id") private final int id;
	@SerializedName("name") private final String name;
	@SerializedName("full_name") private final String fullName;
	@SerializedName("private final") private final boolean privateX;
	@SerializedName("html_url") private final String htmlUrl;
	@SerializedName("description") private final String description;
	@SerializedName("fork") private final boolean fork;
	@SerializedName("url") private final String url;
	@SerializedName("created_at") private final String createdAt;
	@SerializedName("updated_at") private final String updatedAt;
	@SerializedName("pushed_at") private final String pushedAt;
	@SerializedName("homepage") private final String homepage;
	@SerializedName("size") private final int size;
	@SerializedName("owner") private final GithubRepositoryOwnerResponse owner;
	@SerializedName("stargazers_count") private final int stargazersCount;
	@SerializedName("watchers_count") private final int watchersCount;
	@SerializedName("language") private final String language;
	@SerializedName("forks_count") private final int forksCount;
	@SerializedName("open_issues_count") private final int issuesCount;
	@SerializedName("master_branch") private final String masterBranch;
	@SerializedName("default_branch") private final String defaultBranch;
	@SerializedName("score") private final double score;

	public int getId() {
		return id;
	}


	public String getName() {
		return name;
	}


	public String getFullName() {
		return fullName;
	}

	public boolean isPrivateX() {
		return privateX;
	}


	public String getHtmlUrl() {
		return htmlUrl;
	}


	public String getDescription() {
		return description;
	}


	public boolean isFork() {
		return fork;
	}


	public String getUrl() {
		return url;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public String getUpdatedAt() {
		return updatedAt;
	}


	public String getPushedAt() {
		return pushedAt;
	}


	public String getHomepage() {
		return homepage;
	}


	public int getSize() {
		return size;
	}


	public int getStargazersCount() {
		return stargazersCount;
	}


	public int getWatchersCount() {
		return watchersCount;
	}


	public String getLanguage() {
		return language;
	}


	public int getForksCount() {
		return forksCount;
	}


	public int getIssuesCount() {
		return issuesCount;
	}

	public String getMasterBranch() {
		return masterBranch;
	}

	public String getDefaultBranch() {
		return defaultBranch;
	}


	public double getScore() {
		return score;
	}

	public GithubRepositoryResponseItem(int id, String name, String fullName, boolean privateX, String htmlUrl, String description, boolean fork, String url, String createdAt, String updatedAt, String pushedAt, String homepage, int size, GithubRepositoryOwnerResponse owner, int stargazersCount, int watchersCount, String language, int forksCount, int issuesCount, String masterBranch, String defaultBranch, double score) {
		this.id = id;
		this.name = name;
		this.fullName = fullName;
		this.privateX = privateX;
		this.htmlUrl = htmlUrl;
		this.description = description;
		this.fork = fork;
		this.url = url;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.pushedAt = pushedAt;
		this.homepage = homepage;
		this.size = size;
		this.owner = owner;
		this.stargazersCount = stargazersCount;
		this.watchersCount = watchersCount;
		this.language = language;
		this.forksCount = forksCount;
		this.issuesCount = issuesCount;
		this.masterBranch = masterBranch;
		this.defaultBranch = defaultBranch;
		this.score = score;
	}

	public GithubRepositoryOwnerResponse getOwner() {
		return owner;
	}
}

