package com.ianrumac.githubba.search.ui.adapters;

import com.ianrumac.githubba.search.models.GithubRepository;

import java.util.List;

/**
 * Created by ianrumac on 10/04/2017.
 */

public interface SearchResultsAdapter {


	void clearAndAddNewModels(List<GithubRepository> githubRepositories);

	void addModels(List<GithubRepository> githubRepositories);

}
