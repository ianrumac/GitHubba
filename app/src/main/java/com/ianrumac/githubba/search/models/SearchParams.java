package com.ianrumac.githubba.search.models;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchParams {

	public final String query;
	public final Order orderType;
	public final SortBy sortByType;
	public final int page;

	public SearchParams(String query, SortBy sortByType, Order orderType, int page) {
		this.query = query;
		this.orderType = orderType;
		this.sortByType = sortByType;
		this.page = page;
	}


}
