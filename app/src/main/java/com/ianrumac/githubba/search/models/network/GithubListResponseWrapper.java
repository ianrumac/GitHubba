package com.ianrumac.githubba.search.models.network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class GithubListResponseWrapper {

	@SerializedName("total_count") private final int totalCount;
	@SerializedName("incomplete_results") private final boolean incompleteResults;
	@SerializedName("items") private final List<GithubRepositoryResponseItem> itemList;

	public GithubListResponseWrapper(int totalCount, boolean incompleteResults, List<GithubRepositoryResponseItem> itemList) {
		this.totalCount = totalCount;
		this.incompleteResults = incompleteResults;
		this.itemList = itemList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isIncompleteResults() {
		return incompleteResults;
	}

	public List<GithubRepositoryResponseItem> getItemList() {
		return itemList;
	}
}
