package com.ianrumac.githubba.search;

import com.ianrumac.githubba.base.BaseView;
import com.ianrumac.githubba.base.Presenter;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.models.SearchParams;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 08/04/2017.
 */

public interface SearchContract {

	interface SearchView extends BaseView {

		void displayResults(List<GithubRepository> results);

		void appendSearchResults(List<GithubRepository> results);

		Observable<SearchParams> getSearchObservable();

		Observable<SearchParams> getPaginatedObservable();

		void displayEmpty();

		void displayError();

	}

	interface SearchPresenter extends Presenter<SearchView> {

	}

}
