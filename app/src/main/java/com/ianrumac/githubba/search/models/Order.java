package com.ianrumac.githubba.search.models;

/**
 * Created by ianrumac on 08/04/2017.
 */

public enum Order {

	ASCENDING("asc"),
	DESCENDING("desc");

	private final String type;

	Order(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
