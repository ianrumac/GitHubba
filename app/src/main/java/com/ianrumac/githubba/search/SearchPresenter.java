package com.ianrumac.githubba.search;

import com.ianrumac.githubba.search.interactors.SearchRepositoriesUseCase;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.models.SearchParams;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchPresenter implements SearchContract.SearchPresenter {


	private SearchContract.SearchView view;
	private final SearchRepositoriesUseCase useCase;
	private final Scheduler postExecution;
	final int THROTTLE = 500;

	@Inject
	public SearchPresenter(SearchRepositoriesUseCase useCase,
						   @Named("post_execution") Scheduler postExecution) {
		this.postExecution = postExecution;
		this.useCase = useCase;
	}

	@Override
	public void attachView(SearchContract.SearchView view) {
		this.view = view;

		filterAndExecute(view.getSearchObservable())
				.subscribe(this::displayResults, this::handleError);

		filterAndExecute(view.getPaginatedObservable())
				.subscribe(this::appendResults, this::handleError);

	}

	private Observable<List<GithubRepository>> filterAndExecute(Observable<SearchParams> params) {
		return params.filter(searchParams -> searchParams.query.length() > 0)
				.debounce(THROTTLE, TimeUnit.MILLISECONDS)
				.flatMap(useCase::executeWithParams)
				.observeOn(postExecution);

	}


	private void displayResults(List<GithubRepository> results) {
		view.displayResults(results);
		view.hideLoading();
	}

	private void appendResults(List<GithubRepository> results) {
		view.appendSearchResults(results);
		view.hideLoading();
	}


	@Override
	public void handleError(Throwable throwable) {
		view.displayError();
	}


	@Override
	public void detachView(SearchContract.SearchView view) {
		this.view = null;
	}
}
