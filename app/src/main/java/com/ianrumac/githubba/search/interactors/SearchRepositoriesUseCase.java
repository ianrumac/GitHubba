package com.ianrumac.githubba.search.interactors;

import com.ianrumac.githubba.base.UseCase;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.models.SearchParams;
import com.ianrumac.githubba.search.repositories.SearchRepositoryInterface;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchRepositoriesUseCase extends UseCase<List<GithubRepository>, SearchParams> {


	private final SearchRepositoryInterface repository;

	@Inject
	public SearchRepositoriesUseCase(SearchRepositoryInterface repository) {
		this.repository = repository;
	}


	@Override
	public Observable<List<GithubRepository>> executeWithParams(SearchParams query) {
		return repository.getRepositoriesByQuery(query.query, query.sortByType.getValue(), query.orderType.getType(), query.page);

	}

}
