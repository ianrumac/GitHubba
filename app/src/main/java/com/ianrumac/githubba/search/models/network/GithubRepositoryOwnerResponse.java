package com.ianrumac.githubba.search.models.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class GithubRepositoryOwnerResponse {


	/**
	 * login : dtrupenn
	 * id : 872147
	 * avatar_url : https://secure.gravatar.com/avatar/e7956084e75f239de85d3a31bc172ace?d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png
	 * gravatar_id :
	 * url : https://api.github.com/users/dtrupenn
	 * received_events_url : https://api.github.com/users/dtrupenn/received_events
	 * type : User
	 */

	@SerializedName("login") private final String login;
	@SerializedName("id") private final int id;
	@SerializedName("avatar_url") private final String avatarUrl;
	@SerializedName("gravatar_id") private final String gravatarId;
	@SerializedName("url") private final String url;
	@SerializedName("type") private final String type;

	public String getLogin() {
		return login;
	}


	public int getId() {
		return id;
	}


	public String getAvatarUrl() {
		return avatarUrl;
	}


	public String getGravatarId() {
		return gravatarId;
	}


	public String getUrl() {
		return url;
	}


	public String getType() {
		return type;
	}


	public GithubRepositoryOwnerResponse(String login, int id, String avatarUrl, String gravatarId, String url, String type) {
		this.login = login;
		this.id = id;
		this.avatarUrl = avatarUrl;
		this.gravatarId = gravatarId;
		this.url = url;
		this.type = type;
	}
}
