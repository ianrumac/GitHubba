package com.ianrumac.githubba.search.mappers;

import com.annimon.stream.Stream;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.models.Owner;
import com.ianrumac.githubba.search.models.network.GithubListResponseWrapper;
import com.ianrumac.githubba.search.models.network.GithubRepositoryOwnerResponse;
import com.ianrumac.githubba.search.models.network.GithubRepositoryResponseItem;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchResultMapper {

	public Observable<List<GithubRepository>> mapResponseResultToList(Observable<GithubListResponseWrapper> resultWrapperObservable) {

		final List<GithubRepository> repositories = new LinkedList<>();
		return resultWrapperObservable.map(githubListResponseWrapper -> (Stream.of(githubListResponseWrapper.getItemList())
				.map(this::mapToRepository)
				.collect(() -> repositories, List::add)));
	}

	private GithubRepository mapToRepository(GithubRepositoryResponseItem item) {
		return new GithubRepository(item.getId(),
				item.getName(),
				ownerFromItemOwner(item.getOwner()),
				item.getWatchersCount(), item.getForksCount(), item.getIssuesCount());
	}

	private Owner ownerFromItemOwner(GithubRepositoryOwnerResponse itemOwner) {
		return new Owner(itemOwner.
				getLogin(),
				itemOwner.getId(), itemOwner.getAvatarUrl());
	}

}
