package com.ianrumac.githubba.search.di;

/**
 * Created by ianrumac on 10/04/2017.
 */

public interface SearchComponentOwner {

	SearchComponent searchComponent();
}
