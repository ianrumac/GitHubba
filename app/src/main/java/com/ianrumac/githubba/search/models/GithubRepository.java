package com.ianrumac.githubba.search.models;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class GithubRepository {

	private final int id;

	protected final String name;

	protected final Owner author;

	protected final int watchersCount;

	protected final int forkCount;

	protected final int issueCount;

	private GithubRepository(Builder builder) {
		id = builder.id;
		name = builder.name;
		author = builder.author;
		watchersCount = builder.watchersCount;
		forkCount = builder.forkCount;
		issueCount = builder.issueCount;
	}


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Owner getAuthor() {
		return author;
	}


	public int getWatchersCount() {
		return watchersCount;
	}

	public int getForkCount() {
		return forkCount;
	}

	public int getIssueCount() {
		return issueCount;
	}

	@Override
	public String toString() {
		return "SearchResult{" +
				"id=" + id +
				", name='" + name + '\'' +
				", author=" + author +
				", watchersCount=" + watchersCount +
				", forkCount=" + forkCount +
				", issueCount=" + issueCount +
				'}';
	}

	public GithubRepository(int id, String name, Owner author, int watchersCount, int forkCount, int issueCount) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.watchersCount = watchersCount;
		this.forkCount = forkCount;
		this.issueCount = issueCount;
	}


	public static final class Builder {
		private int id;
		private String name;
		private Owner author;
		private int watchersCount;
		private int forkCount;
		private int issueCount;

		public Builder() {
		}

		public Builder id(int val) {
			id = val;
			return this;
		}

		public Builder name(String val) {
			name = val;
			return this;
		}

		public Builder author(Owner val) {
			author = val;
			return this;
		}

		public Builder watchersCount(int val) {
			watchersCount = val;
			return this;
		}

		public Builder forkCount(int val) {
			forkCount = val;
			return this;
		}

		public Builder issueCount(int val) {
			issueCount = val;
			return this;
		}

		public GithubRepository build() {
			return new GithubRepository(this);
		}
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		GithubRepository that = (GithubRepository) o;

		return id == that.id;

	}

	@Override
	public int hashCode() {
		return id;
	}
}
