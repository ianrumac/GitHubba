package com.ianrumac.githubba.search.di;

import com.ianrumac.githubba.core.di.CoreComponent;
import com.ianrumac.githubba.core.di.PerActivity;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.search.ui.SearchActivity;
import com.ianrumac.githubba.utils.ImageLoader;

import javax.inject.Named;

import dagger.Component;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */

@PerActivity
@Component(dependencies = CoreComponent.class, modules = SearchModule.class)
public interface SearchComponent {
	void inject(SearchActivity mainActivity);

	Router provideRouter();

	GithubService service();

	ImageLoader imageLoader();

	@Named("io")
	Scheduler schedulerIO();

	@Named("post_execution")
	Scheduler schedulerPostExecution();

}
