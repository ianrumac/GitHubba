package com.ianrumac.githubba.search.source;

import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.search.mappers.SearchResultMapper;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchNetworkSource implements SearchSource {

	private final GithubService apiService;
	private final SearchResultMapper mapper;

	@Inject
	public SearchNetworkSource(GithubService apiService, SearchResultMapper mapper) {
		this.apiService = apiService;
		this.mapper = mapper;
	}


	@Override
	public Observable<List<GithubRepository>> searchRepositoriesByQuery(String query, String sort, String order, int page) {

		return mapper.mapResponseResultToList(apiService.getRepositoriesForQuery(query, sort, order, page, Constants.PER_PAGE_MAX));
	}
}
