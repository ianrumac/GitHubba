package com.ianrumac.githubba.search.models;

/**
 * Created by ianrumac on 08/04/2017.
 */

public enum SortBy {

	STARS("stars"),
	FORKS("forks"),
	UPDATED("updated");

	private final String value;

	SortBy(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
