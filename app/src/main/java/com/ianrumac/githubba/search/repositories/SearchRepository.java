package com.ianrumac.githubba.search.repositories;

import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.source.SearchSource;

import java.util.List;

import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchRepository implements SearchRepositoryInterface {

	private final SearchSource source;
	private final Scheduler schedulerExecution;

	public SearchRepository(SearchSource source,
							@Named("io") Scheduler execution) {
		this.source = source;
		this.schedulerExecution = execution;
	}

	@Override
	public Observable<List<GithubRepository>> getRepositoriesByQuery(String query, String sort, String order, int page) {
		return source.searchRepositoriesByQuery(query, sort, order, page).subscribeOn(schedulerExecution);
	}


}
