package com.ianrumac.githubba.search.ui.adapters;

import com.airbnb.epoxy.EpoxyAdapter;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.search.models.GithubRepository;
import com.ianrumac.githubba.search.ui.viewmodels.SearchResultViewModel;
import com.ianrumac.githubba.utils.ImageLoader;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ianrumac on 08/04/2017.
 */

public class SearchResultsEpoxyAdapter extends EpoxyAdapter implements SearchResultsAdapter {

	Router router;

	ImageLoader loader;

	@Inject
	public SearchResultsEpoxyAdapter(Router router, ImageLoader loader) {
		this.router = router;
		this.loader = loader;
	}

	@Override
	public void clearAndAddNewModels(List<GithubRepository> githubRepositories) {
		final int modelsSize = models.size();
		models.clear();
		notifyItemRangeRemoved(0, modelsSize);
		addModels(githubRepositories);
	}

	@Override
	public void addModels(List<GithubRepository> githubRepositories) {
		for (int i = 0; i < githubRepositories.size(); i++) {
			GithubRepository result = githubRepositories.get(i);
			addModel(new SearchResultViewModel(result, view -> router.navigateToUserDetails(result.getAuthor().getUsername()),
					view -> router.navigateToRepositoryDetails(result.getAuthor().getUsername(), result.getName()), loader));
			notifyItemInserted(models.size());
		}

	}

}
