package com.ianrumac.githubba.utils;

import android.support.annotation.DrawableRes;
import android.widget.ImageView;

/**
 * Created by ianrumac on 10/04/2017.
 */

public interface ImageLoader {

	void loadImage(ImageView view, String url);

	void loadImageWithPlaceHolder(ImageView view, String url, @DrawableRes int placeHolderResource);

	void loadImageWithPlaceHolderCenterCrop(ImageView view, String url, @DrawableRes int placeHolderResource);

}
