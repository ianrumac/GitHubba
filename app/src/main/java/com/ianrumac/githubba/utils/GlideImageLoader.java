package com.ianrumac.githubba.utils;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

/**
 * Created by ianrumac on 10/04/2017.
 */

public class GlideImageLoader implements ImageLoader {

	Context application;

	@Inject
	public GlideImageLoader(Context application) {
		this.application = application;
	}

	@Override
	public void loadImage(ImageView view, String url) {
		Glide.with(application).load(url).into(view);
	}

	@Override
	public void loadImageWithPlaceHolder(ImageView view, String url, @DrawableRes int placeHolderResource) {
		Glide.with(application).load(url).placeholder(placeHolderResource).into(view);
	}

	@Override
	public void loadImageWithPlaceHolderCenterCrop(ImageView view, String url, @DrawableRes int placeHolderResource) {
		Glide.with(application).load(url).centerCrop().placeholder(placeHolderResource).into(view);
	}


}
