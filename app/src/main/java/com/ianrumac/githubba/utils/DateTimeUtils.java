package com.ianrumac.githubba.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ianrumac on 10/04/2017.
 */

public class DateTimeUtils {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
	private static final SimpleDateFormat output = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

	public static String dayMonthYearFromISODateTime(String date) {
		Date d = new Date();
		try {
			if (date != null) {
				d = sdf.parse(date);
			}
			return output.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
			return date;
		}

	}

}
