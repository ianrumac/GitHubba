package com.ianrumac.githubba.utils;

/**
 * Created by ianrumac on 08/04/2017.
 */

public interface Constants {
	int PER_PAGE_MAX = 30;


	interface Endpoints {
		String SEARCH_REPOSITORIES = "search/repositories";
		String USER_BY_USERNAME = "users/{username}";
		String REPOSITORY_BY_OWNER = "repos/{owner}/{name}";

	}

}
