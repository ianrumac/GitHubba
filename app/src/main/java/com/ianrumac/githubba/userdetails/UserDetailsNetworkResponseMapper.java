package com.ianrumac.githubba.userdetails;

import com.ianrumac.githubba.userdetails.models.User;
import com.ianrumac.githubba.userdetails.models.network.UserResponse;

import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class UserDetailsNetworkResponseMapper {

	public Single<User> mapGithubResultToUser(Single<UserResponse> responseSingle) {

		return responseSingle.map(userResponse -> new User(userResponse.getLogin(), userResponse.getAvatarUrl(), userResponse.getUrl(),
				userResponse.getHtmlUrl(), userResponse.getType(), userResponse.getName(),
				userResponse.getCompany(), userResponse.getBlog(), userResponse.getLocation(),
				userResponse.getEmail(), userResponse.isHireable(), userResponse.getBio(), userResponse.getFollowers()));
	}
}
