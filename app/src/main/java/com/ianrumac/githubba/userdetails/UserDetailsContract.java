package com.ianrumac.githubba.userdetails;

import com.ianrumac.githubba.base.BaseView;
import com.ianrumac.githubba.base.Presenter;
import com.ianrumac.githubba.userdetails.models.User;

/**
 * Created by ianrumac on 09/04/2017.
 */

public interface UserDetailsContract {

	interface UserDetailsView extends BaseView {

		void showUserDetails(User user);
	}

	interface UserDetailsPresenter extends Presenter<UserDetailsView> {

		void fetchAndDisplayUserDetails(String username);

		void displayFullUserDetails(String link);

	}

}
