package com.ianrumac.githubba.userdetails.di;

import com.ianrumac.githubba.core.di.PerFragment;
import com.ianrumac.githubba.search.di.SearchComponent;
import com.ianrumac.githubba.userdetails.ui.UserDetailsFragment;

import dagger.Component;

/**
 * Created by ianrumac on 08/04/2017.
 */

@PerFragment
@Component(dependencies = SearchComponent.class, modules = {UserModule.class})
public interface UserComponent {
	void inject(UserDetailsFragment fragment);
}
