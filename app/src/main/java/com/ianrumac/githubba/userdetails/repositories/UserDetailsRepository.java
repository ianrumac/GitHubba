package com.ianrumac.githubba.userdetails.repositories;

import com.ianrumac.githubba.userdetails.models.User;

import io.reactivex.Single;

/**
 * Created by ianrumac on 10/04/2017.
 */
public interface UserDetailsRepository {
	Single<User> fetchUserProfileData(String username);
}
