package com.ianrumac.githubba.userdetails.models;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class User {
	final String login;
	private final String avatarUrl;
	private final String url;
	private final String htmlUrl;
	private final String type;
	private final String name;
	private final String company;
	private final String blog;
	private final String location;
	private final String email;
	private final boolean hireable;
	private final String bio;
	private final int followers;

	public User(String login, String avatarUrl, String url, String htmlUrl, String type, String name, String company, String blog, String location, String email, boolean hireable, String bio, int followers) {
		this.login = login;
		this.avatarUrl = avatarUrl;
		this.url = url;
		this.htmlUrl = htmlUrl;
		this.type = type;
		this.name = name;
		this.company = company;
		this.blog = blog;
		this.location = location;
		this.email = email;
		this.hireable = hireable;
		this.bio = bio;
		this.followers = followers;
	}

	private User(Builder builder) {
		login = builder.login;
		avatarUrl = builder.avatarUrl;
		url = builder.url;
		htmlUrl = builder.htmlUrl;
		type = builder.type;
		name = builder.name;
		company = builder.company;
		blog = builder.blog;
		location = builder.location;
		email = builder.email;
		hireable = builder.hireable;
		bio = builder.bio;
		followers = builder.followers;
	}


	public String getAvatarUrl() {
		return avatarUrl;
	}

	public String getUrl() {
		return url;
	}

	public String getHtmlUrl() {
		return htmlUrl;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getCompany() {
		return company;
	}

	public String getBlog() {
		return blog;
	}

	public String getLocation() {
		return location;
	}

	public String getEmail() {
		return email;
	}

	public boolean isHireable() {
		return hireable;
	}

	public String getBio() {
		return bio;
	}

	public int getFollowers() {
		return followers;
	}

	public String getLogin() {
		return login;
	}

	public static final class Builder {
		private String login = "";
		private String avatarUrl = "";
		private String url = "";
		private String htmlUrl = "";
		private String type = "";
		private String name = "";
		private String company = "";
		private String blog = "";
		private String location = "";
		private String email = "";
		private boolean hireable = false;
		private String bio = "";
		private int followers = 0;

		public Builder() {
		}

		public Builder login(String val) {
			login = val;
			return this;
		}

		public Builder avatarUrl(String val) {
			avatarUrl = val;
			return this;
		}

		public Builder url(String val) {
			url = val;
			return this;
		}

		public Builder htmlUrl(String val) {
			htmlUrl = val;
			return this;
		}

		public Builder type(String val) {
			type = val;
			return this;
		}

		public Builder name(String val) {
			name = val;
			return this;
		}

		public Builder company(String val) {
			company = val;
			return this;
		}

		public Builder blog(String val) {
			blog = val;
			return this;
		}

		public Builder location(String val) {
			location = val;
			return this;
		}

		public Builder email(String val) {
			email = val;
			return this;
		}

		public Builder hireable(boolean val) {
			hireable = val;
			return this;
		}

		public Builder bio(String val) {
			bio = val;
			return this;
		}

		public Builder followers(int val) {
			followers = val;
			return this;
		}

		public User build() {
			return new User(this);
		}
	}
}
