package com.ianrumac.githubba.userdetails.source;

import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.userdetails.UserDetailsNetworkResponseMapper;
import com.ianrumac.githubba.userdetails.models.User;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class UserDataNetworkSource implements UserDataSource {


	GithubService apiService;
	UserDetailsNetworkResponseMapper responseMapper;

	@Inject
	public UserDataNetworkSource(GithubService apiService, UserDetailsNetworkResponseMapper mapper) {
		this.apiService = apiService;
		this.responseMapper = mapper;
	}

	@Override
	public Single<User> getFullUserProfileByUsername(String username) {

		return responseMapper.mapGithubResultToUser(apiService.getUserByUsername(username));
	}
}
