package com.ianrumac.githubba.userdetails.interactors;

import com.ianrumac.githubba.base.UseCase;
import com.ianrumac.githubba.userdetails.models.User;
import com.ianrumac.githubba.userdetails.repositories.UserDetailsRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class GetUserDetailsUseCase extends UseCase<User, String> {

	private final UserDetailsRepository repository;

	@Inject
	public GetUserDetailsUseCase(UserDetailsRepository repository) {
		this.repository = repository;
	}

	@Override
	public Observable<User> executeWithParams(String query) {
		return repository.fetchUserProfileData(query).toObservable();
	}
}
