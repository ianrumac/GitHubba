package com.ianrumac.githubba.userdetails.repositories;

import com.ianrumac.githubba.userdetails.models.User;
import com.ianrumac.githubba.userdetails.source.UserDataSource;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class UserRepository implements UserDetailsRepository {

	UserDataSource dataSource;
	Scheduler executionScheduler;

	@Inject
	public UserRepository(UserDataSource dataSource,
						  @Named("io") Scheduler execution) {
		this.dataSource = dataSource;
		this.executionScheduler = execution;
	}

	@Override
	public Single<User> fetchUserProfileData(String username) {
		return dataSource.getFullUserProfileByUsername(username).subscribeOn(executionScheduler);
	}
}
