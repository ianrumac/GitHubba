package com.ianrumac.githubba.userdetails.models.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class UserResponse {


	/**
	 * login : octocat
	 * id : 1
	 * avatar_url : https://github.com/images/error/octocat_happy.gif
	 * gravatar_id :
	 * url : https://api.github.com/users/octocat
	 * html_url : https://github.com/octocat
	 * followers_url : https://api.github.com/users/octocat/followers
	 * following_url : https://api.github.com/users/octocat/following{/other_user}
	 * gists_url : https://api.github.com/users/octocat/gists{/gist_id}
	 * starred_url : https://api.github.com/users/octocat/starred{/owner}{/repo}
	 * subscriptions_url : https://api.github.com/users/octocat/subscriptions
	 * organizations_url : https://api.github.com/users/octocat/orgs
	 * repos_url : https://api.github.com/users/octocat/repos
	 * events_url : https://api.github.com/users/octocat/events{/privacy}
	 * received_events_url : https://api.github.com/users/octocat/received_events
	 * type : User
	 * site_admin : false
	 * name : monalisa octocat
	 * company : GitHub
	 * blog : https://github.com/blog
	 * location : San Francisco
	 * email : octocat@github.com
	 * hireable : false
	 * bio : There once was...
	 * public_repos : 2
	 * public_gists : 1
	 * followers : 20
	 * following : 0
	 * created_at : 2008-01-14T04:33:35Z
	 * updated_at : 2008-01-14T04:33:35Z
	 */

	@SerializedName("login") private final String login;
	@SerializedName("id") private final int id;
	@SerializedName("avatar_url") private final String avatarUrl;
	@SerializedName("gravatar_id") private final String gravatarId;
	@SerializedName("url") private final String url;
	@SerializedName("html_url") private final String htmlUrl;
	@SerializedName("type") private final String type;
	@SerializedName("name") private final String name;
	@SerializedName("company") private final String company;
	@SerializedName("blog") private final String blog;
	@SerializedName("location") private final String location;
	@SerializedName("email") private final String email;
	@SerializedName("hireable") private final boolean hireable;
	@SerializedName("bio") private final String bio;
	@SerializedName("public_repos") private final int publicRepos;
	@SerializedName("public_gists") private final int publicGists;
	@SerializedName("followers") private final int followers;
	@SerializedName("following") private final int following;
	@SerializedName("created_at") private final String createdAt;
	@SerializedName("updated_at") private final String updatedAt;

	public UserResponse(String login, int id, String avatarUrl, String gravatarId, String url, String htmlUrl, String type, String name, String company, String blog, String location, String email, boolean hireable, String bio, int publicRepos, int publicGists, int followers, int following, String createdAt, String updatedAt) {
		this.login = login;
		this.id = id;
		this.avatarUrl = avatarUrl;
		this.gravatarId = gravatarId;
		this.url = url;
		this.htmlUrl = htmlUrl;
		this.type = type;
		this.name = name;
		this.company = company;
		this.blog = blog;
		this.location = location;
		this.email = email;
		this.hireable = hireable;
		this.bio = bio;
		this.publicRepos = publicRepos;
		this.publicGists = publicGists;
		this.followers = followers;
		this.following = following;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public String getLogin() {
		return login;
	}


	public int getId() {
		return id;
	}


	public String getAvatarUrl() {
		return avatarUrl;
	}


	public String getGravatarId() {
		return gravatarId;
	}


	public String getUrl() {
		return url;
	}


	public String getHtmlUrl() {
		return htmlUrl;
	}


	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getCompany() {
		return company;
	}

	public String getBlog() {
		return blog;
	}


	public String getLocation() {
		return location;
	}

	public String getEmail() {
		return email;
	}

	public boolean isHireable() {
		return hireable;
	}


	public String getBio() {
		return bio;
	}


	public int getPublicRepos() {
		return publicRepos;
	}


	public int getPublicGists() {
		return publicGists;
	}

	public int getFollowers() {
		return followers;
	}


	public int getFollowing() {
		return following;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public String getUpdatedAt() {
		return updatedAt;
	}
}
