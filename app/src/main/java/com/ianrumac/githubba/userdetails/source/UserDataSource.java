package com.ianrumac.githubba.userdetails.source;

import com.ianrumac.githubba.userdetails.models.User;

import io.reactivex.Single;

/**
 * Created by ianrumac on 09/04/2017.
 */

public interface UserDataSource {

	Single<User> getFullUserProfileByUsername(String username);
}
