package com.ianrumac.githubba.userdetails.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ianrumac.githubba.R;
import com.ianrumac.githubba.base.BaseFragment;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.search.di.SearchComponentOwner;
import com.ianrumac.githubba.userdetails.UserDetailsContract;
import com.ianrumac.githubba.userdetails.di.DaggerUserComponent;
import com.ianrumac.githubba.userdetails.di.UserModule;
import com.ianrumac.githubba.userdetails.models.User;
import com.ianrumac.githubba.utils.ImageLoader;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserDetailsFragment extends BaseFragment implements UserDetailsContract.UserDetailsView {


	private static final String USERNAME = "username";
	@BindView(R.id.user_profile_photo) ImageView userProfilePhoto;
	@BindView(R.id.location) TextView location;
	@BindView(R.id.email) TextView email;
	@BindView(R.id.bio) TextView bio;
	@BindView(R.id.link) TextView link;
	@BindView(R.id.user_name) TextView userNameView;


	@Inject UserDetailsContract.UserDetailsPresenter presenter;
	@Inject ImageLoader loader;
	@Inject Router router;

	public static UserDetailsFragment newInstance(String userName) {
		UserDetailsFragment fragment = new UserDetailsFragment();
		Bundle args = new Bundle();
		args.putString(USERNAME, userName);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_user_details, container, false);
		ButterKnife.bind(this, view);
		DaggerUserComponent.builder().searchComponent(((SearchComponentOwner) getActivity()).searchComponent()).userModule(new UserModule()).build().inject(this);
		presenter.attachView(this);
		if (getArguments() != null) {
			presenter.fetchAndDisplayUserDetails(getArguments().getString(USERNAME));
		}
		return view;


	}


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void showLoading() {
		userNameView.setText(R.string.loading);
		location.setText(R.string.loading);
		bio.setText(R.string.loading);
	}

	@Override
	public void hideLoading() {

	}

	@Override
	public void displayError() {
		Toast.makeText(getContext(), R.string.error_msg, Toast.LENGTH_LONG).show();

	}

	@OnClick(R.id.back)
	void backPressed() {
		router.goBack();
	}

	@Override
	public void showUserDetails(User user) {
		loader.loadImage(userProfilePhoto, user.getAvatarUrl());
		setTextIfNotEmptyOrElse(user.getName(), userNameView, R.string.unknown);
		setTextIfNotEmptyOrElse(user.getLocation(), location, getString(R.string.location_unknown));
		setTextIfNotEmptyOrElse(user.getBio(), bio, getString(R.string.no_bio_found));
		setTextIfNotEmptyOrElse(user.getEmail(), email, getString(R.string.no_email));
		link.setOnClickListener(view -> presenter.displayFullUserDetails(user.getHtmlUrl()));
	}


	@Override
	public void onDestroyView() {
		presenter.detachView(this);
		super.onDestroyView();
	}
}
