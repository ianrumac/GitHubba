package com.ianrumac.githubba.userdetails;

import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.repodetails.RepositoryDetailsContract;
import com.ianrumac.githubba.repodetails.interactors.GetRepositoryDetailsUseCase;
import com.ianrumac.githubba.repodetails.models.GithubRepositoryInfo;

import javax.inject.Inject;

import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class RepositoryDetailsPresenter implements RepositoryDetailsContract.RepositoryDetailsPresenter {

	RepositoryDetailsContract.RepositoryDetailsView view;
	private final Scheduler postExecutionScheduler;
	private final GetRepositoryDetailsUseCase repositoryDetailsUseCase;
	private final Router router;

	@Inject
	public RepositoryDetailsPresenter(Scheduler postExecutionScheduler, GetRepositoryDetailsUseCase repositoryDetailsUseCase, Router router) {
		this.postExecutionScheduler = postExecutionScheduler;
		this.repositoryDetailsUseCase = repositoryDetailsUseCase;
		this.router = router;
	}


	@Override
	public void attachView(RepositoryDetailsContract.RepositoryDetailsView view) {
		this.view = view;
	}

	@Override
	public void detachView(RepositoryDetailsContract.RepositoryDetailsView view) {
		this.view = null;
	}

	@Override
	public void handleError(Throwable throwable) {
		throwable.printStackTrace();
		if (view != null) {
			view.displayError();
		}
	}

	private void displayRepositoryDetails(GithubRepositoryInfo details) {
		if (view != null) {
			view.showRepositoryDetails(details);
		}
	}

	@Override
	public void fetchAndDisplayRepositoryData(String owner, String repositoryName) {
		repositoryDetailsUseCase
				.executeWithParams(new GetRepositoryDetailsUseCase.Params(owner, repositoryName))
				.observeOn(postExecutionScheduler)
				.subscribe(this::displayRepositoryDetails, this::handleError);
	}

	@Override
	public void displayOwnerDetails(String owner) {
		router.navigateToUserDetails(owner);
	}

	@Override
	public void displayFullRepository(String url) {
		router.openLinkInBrowser(url);
	}
}
