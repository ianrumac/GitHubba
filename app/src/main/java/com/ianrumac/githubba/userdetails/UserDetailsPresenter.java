package com.ianrumac.githubba.userdetails;

import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.userdetails.interactors.GetUserDetailsUseCase;
import com.ianrumac.githubba.userdetails.models.User;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class UserDetailsPresenter implements UserDetailsContract.UserDetailsPresenter {

	Scheduler postExecution;
	GetUserDetailsUseCase userDetailsUseCase;
	UserDetailsContract.UserDetailsView view;
	Router router;

	@Inject
	public UserDetailsPresenter(GetUserDetailsUseCase userDetailsUseCase, @Named("post_execution") Scheduler postExecution, Router router) {
		this.postExecution = postExecution;
		this.userDetailsUseCase = userDetailsUseCase;
		this.router = router;
	}

	@Override
	public void attachView(UserDetailsContract.UserDetailsView view) {
		this.view = view;
		if (view != null) {
			view.showLoading();
		}
	}

	@Override
	public void detachView(UserDetailsContract.UserDetailsView view) {
		this.view = null;
	}

	@Override
	public void handleError(Throwable throwable) {
		throwable.printStackTrace();

	}

	private void displayUserDetails(User user) {
		if (view != null) {
			view.hideLoading();
			view.showUserDetails(user);
		}
	}


	@Override
	public void fetchAndDisplayUserDetails(String username) {
		userDetailsUseCase.executeWithParams(username)
				.observeOn(postExecution)
				.subscribe(this::displayUserDetails, this::handleError);
	}

	@Override
	public void displayFullUserDetails(String link) {
		router.openLinkInBrowser(link);
	}
}
