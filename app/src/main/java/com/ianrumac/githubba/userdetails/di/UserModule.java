package com.ianrumac.githubba.userdetails.di;

import com.ianrumac.githubba.core.di.PerFragment;
import com.ianrumac.githubba.navigation.Router;
import com.ianrumac.githubba.networking.GithubService;
import com.ianrumac.githubba.userdetails.UserDetailsContract;
import com.ianrumac.githubba.userdetails.UserDetailsNetworkResponseMapper;
import com.ianrumac.githubba.userdetails.UserDetailsPresenter;
import com.ianrumac.githubba.userdetails.interactors.GetUserDetailsUseCase;
import com.ianrumac.githubba.userdetails.repositories.UserDetailsRepository;
import com.ianrumac.githubba.userdetails.repositories.UserRepository;
import com.ianrumac.githubba.userdetails.source.UserDataNetworkSource;
import com.ianrumac.githubba.userdetails.source.UserDataSource;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by ianrumac on 08/04/2017.
 */


@Module
public class UserModule {


	@Provides
	@PerFragment
	UserDetailsNetworkResponseMapper provideResultMapper() {
		return new UserDetailsNetworkResponseMapper();
	}

	@Provides
	@PerFragment
	UserDataSource provideSource(GithubService service, UserDetailsNetworkResponseMapper mapper) {
		return new UserDataNetworkSource(service, mapper);
	}

	@Provides
	@PerFragment
	UserDetailsRepository provideRepository(UserDataSource source, @Named("io") Scheduler io) {
		return new UserRepository(source, io);
	}

	@Provides
	@PerFragment
	GetUserDetailsUseCase provideUserDetailsUseCase(UserDetailsRepository repository) {
		return new GetUserDetailsUseCase(repository);
	}

	@Provides
	@PerFragment
	UserDetailsContract.UserDetailsPresenter providePresenter(Router router, GetUserDetailsUseCase repositoriesUseCase, @Named("post_execution") Scheduler postExecution) {
		return new UserDetailsPresenter(repositoriesUseCase, postExecution, router);
	}


}
