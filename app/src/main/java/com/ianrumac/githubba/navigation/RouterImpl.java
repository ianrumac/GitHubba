package com.ianrumac.githubba.navigation;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.view.inputmethod.InputMethodManager;

import com.ianrumac.githubba.R;
import com.ianrumac.githubba.repodetails.ui.RepositoryDetailsFragment;
import com.ianrumac.githubba.userdetails.ui.UserDetailsFragment;

/**
 * Created by ianrumac on 09/04/2017.
 */

public class RouterImpl implements Router {


	private final Activity activity;
	private final FragmentManager fragmentManager;

	public RouterImpl(final Activity activity, final FragmentManager fragmentManager) {
		this.activity = activity;
		this.fragmentManager = fragmentManager;
	}

	public void hideKeyboardWhenNavigating() {
		try {
			InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void navigateToUserDetails(String username) {
		hideKeyboardWhenNavigating();
		fragmentManager.beginTransaction()
				.add(R.id.container_layout, UserDetailsFragment.newInstance(username))
				.addToBackStack(username)
				.commit();

	}

	@Override
	public void navigateToRepositoryDetails(String owner, String name) {
		hideKeyboardWhenNavigating();
		fragmentManager.beginTransaction()
				.add(R.id.container_layout, RepositoryDetailsFragment.newInstance(owner, name))
				.addToBackStack(owner.concat(name))
				.commit();

	}

	@Override
	public void goBack() {
		fragmentManager.popBackStack();

	}

	@Override
	public void openLinkInBrowser(String link) {
		activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

	}

	@Override
	public void clearBackstack() {

	}
}
