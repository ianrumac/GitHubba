package com.ianrumac.githubba.navigation;

/**
 * Created by ianrumac on 09/04/2017.
 */

public interface Router {

	public void navigateToUserDetails(String username);

	public void navigateToRepositoryDetails(String owner, String name);

	public void goBack();

	public void openLinkInBrowser(String link);

	public void clearBackstack();

}
